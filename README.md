# Bootstrap

On a new system as root, run: `git clone https://gitlab.com/jhizzle84/bootstrap.git /root/bootstrap`

Run the bootstrap script: `cd /root/bootstrap; ./bootstrap.sh --help`

Remove the bootstrap script: `cd; rm -rf bootstrap`
