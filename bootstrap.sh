#!/bin/bash
function show_help {
  echo "Usage: ./bootstrap.sh [--clonerepos]";
  exit 0;
}

CLONEREPOS=false;

if [ $# -eq 1 ]; then
  if [[ "$1" == "--help" ]]; then
    show_help
  elif [[ "$1" == "--clonerepos" ]]; then
    CLONEREPOS=true;
  else
    show_help
  fi
fi

if [ $# -gt 1 ]; then
  show_help
fi

SWAP=`free -m | grep 'Swap' | awk '{print $2}'`;

if [ $SWAP -gt 0 ]; then
  echo "WARNING: Disable swap permanently then try again!";
  exit 1;
fi

echo "Setting up ansible user with sudo and private key access...";
yum -y install sudo >/dev/null 2>&1;
useradd ansible >/dev/null >/dev/null 2>&1;
passwd -l ansible >/dev/null 2>&1;
cp files/ansible.sudoers /etc/sudoers.d >/dev/null 2>&1;
mkdir -p /home/ansible/.ssh >/dev/null 2>&1;
cp files/id_rsa.pub /home/ansible/.ssh/authorized_keys >/dev/null 2>&1;
chown -R ansible:ansible /home/ansible >/dev/null 2>&1;
chmod 0700 /home/ansible/.ssh >/dev/null 2>&1;
chmod 0644 /home/ansible/.ssh/authorized_keys >/dev/null 2>&1;

echo "Installing ansible...";
yum -y install python python-pip 2>/dev/null >/dev/null;
pip install ansible 2>/dev/null >/dev/null;

echo -e "\nExecuting Ansible...";
ansible-playbook --ask-vault-pass -e clonerepos=${CLONEREPOS} site.yml && echo -e "\nYou can now remove the bootstrap directory";
